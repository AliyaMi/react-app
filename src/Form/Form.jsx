import { Link } from "react-router-dom";
import Button from "../components/Button/Button";
import './Form.scss';

export default function Form({header, buttonText, linkText, linkTo}) {
    return  (
        <form className="form">
            <Link to={linkTo} href="#" className="form__link">
                {linkText}
                </Link>
            <h1 class="form__header" >{header}</h1>
            <fieldset class="form__fieldset">
                <input placeholder="Логин" className="form__input" type="text"></input>
            </fieldset>
            <fieldset class="form__fieldset">
            <input placeholder="Пароль" className="form__input" type="password"></input>
            </fieldset>
            <fieldset class="form__fieldset form__checkbox-wrap">
            <input className="form__checkbox" type="checkbox"></input>
            <span className="form__checkbox-text">Я согласен получать обновления на почту</span>
            </fieldset>
            <div class="form__btn-wrap">
            <Button>{buttonText}</Button>
            </div>
        </form>
    );
}

