import { BrowserRouter, Routes, Route } from "react-router-dom"
import './App.css';
import AuthPage from './pages/AuthPage/AuthPage';
import ProductListPage from './pages/ProductListPage/ProductListPage';
import ProductPage from './pages/ProductPage/ProductPage';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<ProductListPage />} />
      <Route path="signIn" element={<AuthPage signIn />} />
      <Route path="/signUp" element={<AuthPage signUp />} />
      <Route path="/product/:productId" element={<ProductPage />} />
    </Routes>
    </BrowserRouter>
  );
}

export default App;
