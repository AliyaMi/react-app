import './Button.scss'

export default function Button({children, outline}) {
    const button = outline ? 'button button_outline' : 'button';
    return (
        <button className={button}>
    {children}
    </button>
    );
}
    

/*export default function Button({children, outline}) {

    return (
        <button className={'button' + (outline ? 'button__outline' : '')}>
            {children}
        </button>
    );
} */