import './Sum.scss';
import React, {useState } from "react";

export default function Sum({}) {
    const [cart, setCart] = useState([]);
    const cartTotalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price , 0)
    const addToCart = (product) => {
        setCart((prevCart) => [...prevCart, product])
    }
    const ending = (int, array) => {
        return (array = array || ['товар', 'товара', 'товаров']) && array[(int % 100 > 4 && int % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(int % 10 < 5) ? int % 10 : 5]];
    }

    return (
            <span className={'sum'}>
            {cart.length} {ending(cart.length)} <br/> на сумму {cartTotalPrice} Р
        </span>
    );
}
