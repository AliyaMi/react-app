import classes from './ProductPage.module.scss';
import styled from "styled-components";
import Button from "../../components/Button/Button";
//import { Button as MaterialButton } from '@mui/material';
import Sum from '../../components/Sum/Sum';
import BasketImg from '../../components/BasketImg/BasketImg';
import BackImage from '../../components/BackImage/BackImage';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import {productList} from '../../utils/data';



const Image = styled.img `
    width: 100%;
    height: auto;
    max-width: 500px;
`;

const Title = styled.h1`
    font-style: normal;
    font-weight: 500;
    font-size: 30px;
    line-height: 37px;
    color: #D58C51;
    `;
const Text = styled.p `
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #FFFFFF;
`;
const Price = styled.span`
    font-style: normal;
    font-weight: 500;
    font-size: 23px;
    line-height: 28px;
    color: #FFFFFF;
    white-space: pre;
`;
const Weight = styled.span `
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 22px;
    color: #FFFFFF;
    white-space: pre;
`;
const Divider = styled.span `
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 22px;
    color: #FFFFFF;
    margin: 0 8px;
`;
/*
const HeaderButton = (props) => (
    <MaterialButton sx={{ 
        border: "1px solid #D58C51",
        color: "#D58C51",
        marginLeft:"20px",
        }}>
        {props.children}
    </MaterialButton>
);
*/

export default function ProductPage() {
    const {productId} = useParams();
    const [product, setProduct] = useState({});

    useEffect(() => {
        const p = productList.find((e) => e.id === Number(productId));
        if (p) {
            setProduct(p);
        } else {
            // redirect 404
        }
    }, [productId]);

return (
    <div className={classes.productPage}>
        <div className={classes.productPage__header}>
            <div><BackImage></BackImage></div>
            <div className={classes.productPage__headerRight}>
                <Sum></Sum>
                <div className={classes.basketWrap}><BasketImg></BasketImg></div>
                <Button outline>Выйти</Button>
            </div>
        </div>
        <main className={classes.productPage__main}>
            <div 
            style={{
            marginRight: '100px',
            }}
            >
                <Image src={product.image}/>
            </div>
            <div>
                <Title>{product.title}</Title>
                <Text>{product.text}</Text>
                <div className={classes.priceWrap}>
                    <Price>{product.price?.toLocaleString('ru')} Р</Price>
                    <Divider>/</Divider>
                    {Boolean(product.weight) && <Weight>{product.weight} г.</Weight>}
                    {Boolean(product.amount) && <Weight>{product.amount} шт.</Weight>}
                    <div className={classes.btnWrap}>
                    <Button>В корзину</Button>
                    </div>
                </div>
            </div>
        </main>
    </div>
);

}