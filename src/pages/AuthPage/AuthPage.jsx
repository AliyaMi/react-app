import Form from '../../Form/Form';
import './AuthPage.scss';

export default function AuthPage({signIn, signUp}) {
    return (
        <div className="auth-page">
      {signIn &&
        <Form
        header={'вход'}
        linkText='Зарегистрироваться'
        linkTo="/signUp"
        buttonText={'Войти'}
      />
      }
      {signUp &&
        <Form
        header={'Регистрация'}
        linkText='Авторизироваться'
        linkTo="/signIn"
        buttonText={'Зарегистрироваться'}
      />
      }
        </div>
    )
}