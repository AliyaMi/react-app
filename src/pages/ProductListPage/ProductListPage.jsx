import './ProductListPage.scss';
import {productList} from '../../utils/data'
import ProductCard from '../../components/ProductCard/ProductCard';
import React, {useState } from "react";

export default function ProductListPage() {
   
    const [cart, setCart] = useState([]);

    const cartTotalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price , 0)

    const addToCart = (product) => {
        setCart((prevCart) => [...prevCart, product])
    }

    const ending = (int, array) => {
        return (array = array || ['товар', 'товара', 'товаров']) && array[(int % 100 > 4 && int % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(int % 10 < 5) ? int % 10 : 5]];
    }
    
return (
    <div>
    <header className="header">
       <h1 className="header__title">наша продукция</h1> 
       <div className="header__cart-block">
        <span>
            {cart.length} {ending(cart.length)} <br />на сумму {cartTotalPrice} Р
        </span>
       </div>
    </header>
    <main className="product-list-container">
        {productList.map((product) => (
            <div key={product.id} className="product-list-item">
                <ProductCard 
            id={product.id}
            title={product.title} 
            text={product.text} 
            image={product.image}
            price={product.price} 
            weight={product.weight} 
            amount={product.amount}
            onButtonClick={() => addToCart(product)}
            />
            </div>
))}
    </main>
    </div>
);

}